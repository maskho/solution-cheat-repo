/*
1. The Candies
Alice is a kindergarten teacher. She wants to give some candies to the children in her class. All
the children sit in a line and each of them has a rating score according to his or her performance
in the class. Alice wants to give at least 1 candy to each child. If two children sit next to each
other, then the one with the higher rating must get more candies. Alice wants to minimize the
total number of candies she must buy.
For example, assume her students' ratings are [4, 6, 4, 5, 6, 2]. She gives the students candy in
the following minimal amounts: [1, 2, 1, 2, 3, 1]. She must buy a minimum of 10 candies

Sample 1:
Input
3 1 2 2
Output
4

Sample 2:
Input
10 2 4 2 6 1 7 8 9 2 1
Output
19
*/

import java.util.Scanner;

public class Deloitte1 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();
        int[] arr = new int[n];
        int[] candies = new int[n];

        for (int i = 0; i < n; i++) {
            arr[i] = scanner.nextInt();
            candies[i] = 1;
        }
        scanner.close();

        for (int i = 1; i < n; i++) {
            if (arr[i] > arr[i - 1]) {
                candies[i] = candies[i - 1] + 1;
            }
        }

        int sum = candies[n - 1];
        for (int i = n - 2; i >= 0; i--) {
            if (arr[i] > arr[i + 1] && candies[i] < candies[i + 1] + 1) {
                candies[i] = candies[i + 1] + 1;
            }
            sum = sum + candies[i];
        }
        System.out.print(sum);
    }
}
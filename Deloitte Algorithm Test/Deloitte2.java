
/*
2. Left Rotation
A left rotation operation on an	array shifts each of the array's elements unit to the left. For	example,	
if left	rotations are performed on array [1,2,3,4,5], then the array would become [3,4,5,1,2].
Given	an	array a	of n integers and a	number, d, perform d left rotations	on the array. Return the updated array to be printed as	a single line of space-separated integers.

Sample Input
[1 2 3 4 5], 4

Sample Output
[5 1 2 3 4]
*/
import java.util.Arrays;
import java.util.Scanner;

public class Deloitte2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        String input = scanner.nextLine();

        String data = input.split(",")[0].replaceAll("\\W", "");
        int[] arr = new int[data.length()];
        int rot = Integer.parseInt(input.split(",")[1].replaceAll("\\s+", ""));

        for (int i = 0; i < data.length(); i++) {
            arr[(i + data.length() - rot) % data.length()] = Integer.parseInt(Character.toString(data.charAt(i)));
        }
        System.out.println(Arrays.toString(arr));
    }
}
